# xDEFI Homework

CAUTION!!! Application works on MAINNET

## Dev notes

1. Components might be more granular.
2. Data fetching from the root container has its pros and cons.

        - Pros: It makes other components dumb and easily testable. It helps to avoid waterfall requests. 
        - Cons: Unnecessary renders. Component bloating.

3. Ether price and transaction history requests make sense to get as server-side props. I made it with Zustand to demonstrate work with state management and loading transitions.

4. Connect wallet button works just with injectable wallets for simplicity

5. Hydration problem with the connected wallet has been solved with `useEffect` in a particular component. It could be solved globally with some kind of wrapper.

## Pre-install

Populate environment variables

copy `.env.example` to `.env.local`

## Install dependencies

```bash
yarn
```

## Run application

```bash
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Run tests

```bash
yarn run test
```
