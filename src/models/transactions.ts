import { create } from "zustand";
import { AssetTransfersCategory, SortingOrder } from "alchemy-sdk";
import { alchemy } from "@/services/alchemy";

export type Transactions = Awaited<
  ReturnType<typeof alchemy.core.getAssetTransfers>
>["transfers"];

interface TransactionState {
  transactions: Transactions;
  loading: boolean;
  fetch: (address?: `0x${string}`) => void;
}

const useTransactionStore = create<TransactionState>((set) => ({
  transactions: [],
  loading: false,
  fetch: (address) => {
    if (!address) {
      return set({ transactions: [] });
    }
    set({ loading: true });
    alchemy.core
      .getAssetTransfers({
        maxCount: 5,
        order: SortingOrder.DESCENDING,
        fromBlock: "0x0",
        fromAddress: address,
        category: [
          AssetTransfersCategory.EXTERNAL,
          AssetTransfersCategory.INTERNAL,
          AssetTransfersCategory.ERC20,
        ],
      })
      .then((data) => {
        set({ transactions: data.transfers });
      })
      .finally(() => set({ loading: false }));
  },
}));

export default useTransactionStore;
