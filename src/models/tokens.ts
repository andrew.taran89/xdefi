import { create } from "zustand";

interface TokenState {
  tokens?: string;
  loading: boolean;
  setLoading: (loading: boolean) => void;
  setTokens: (loading?: string) => void;
}

const useTokensStore = create<TokenState>((set) => ({
  loading: false,
  setLoading: (loading: boolean) => set({ loading }),
  setTokens: (tokens?: string) => set({ tokens }),
}));

export default useTokensStore;
