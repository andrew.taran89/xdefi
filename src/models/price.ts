import { create } from "zustand";

interface PriceState {
  price: number;
  loading: boolean;
  fetch: () => void;
}

const usePriceStore = create<PriceState>((set) => ({
  price: 0,
  loading: false,
  fetch: () => {
    set({ loading: true });
    fetch(
      "https://api.coingecko.com/api/v3/simple/price?ids=ethereum&vs_currencies=usd"
    )
      .then((res) => res.json())
      .then((data) => {
        set({ price: data.ethereum.usd });
      })
      .finally(() => set({ loading: false }));
  },
}));

export default usePriceStore;
