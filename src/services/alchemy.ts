import config from "@/config";
import { Alchemy, Network } from "alchemy-sdk";

export const alchemy = new Alchemy({
  apiKey: config.alchemyApiKey,
  network: Network.ETH_MAINNET,
});
