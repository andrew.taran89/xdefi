import { createClient, configureChains, mainnet } from "wagmi";
import { alchemyProvider } from "wagmi/providers/alchemy";
import { publicProvider } from "wagmi/providers/public";
import { InjectedConnector } from "wagmi/connectors/injected";
import config from "@/config";

const { provider, chains, webSocketProvider } = configureChains(
  [mainnet],
  [alchemyProvider({ apiKey: config.alchemyApiKey }), publicProvider()]
);

export const client = createClient({
  autoConnect: true,
  provider,
  connectors: [new InjectedConnector({ chains })],
  webSocketProvider,
});
