import NextHead from "next/head";

function Head() {
  return (
    <NextHead>
      <title>xDEFI Homework</title>
      <meta name="description" content="DAPP" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <link rel="icon" href="/favicon.ico" />
    </NextHead>
  );
}

export default Head;
