import { render, screen } from "@testing-library/react";
import TransactionList from "../index";
import "@testing-library/jest-dom";
import type { Transactions } from "@/models/transactions";

describe("TransactionList", () => {
  const transactions = [
    {
      blockNum: "0x123123",
      hash: "0x3be40837f6b652ee3fc3313d86e269151dc235d04191d49dde42c45dcbd7d228",
      to: "0xb3f723783e67471F2469C3fc49E2905d5FabeE1d",
    },
  ] as Transactions;
  it("renders explorer links", () => {
    render(
      <TransactionList
        transactions={transactions}
        loading={false}
        explorerUrl="https://etherscan.io"
      />
    );

    const trxHref = `https://etherscan.io/tx/${transactions[0].hash}`;
    const recipientHref = `https://etherscan.io/address/${transactions[0].to}`;

    const [trxUrl, recipientUrl] =
      screen.getAllByRole<HTMLAnchorElement>("link");

    expect(trxUrl).toHaveAttribute("href", trxHref);
    expect(recipientUrl).toHaveAttribute("href", recipientHref);
  });

  it("renders loader", () => {
    render(
      <TransactionList
        transactions={transactions}
        loading={true}
        explorerUrl="https://etherscan.io"
      />
    );

    const loading = screen.getByText("Loading...");

    expect(loading).toBeInTheDocument();
  });
});
