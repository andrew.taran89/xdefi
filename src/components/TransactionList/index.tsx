import type { Transactions } from "@/models/transactions";

interface TransactionListProps {
  transactions?: Transactions;
  loading: boolean;
  explorerUrl: string;
}

function TransactionList({
  transactions = [],
  loading,
  explorerUrl,
}: TransactionListProps) {
  return (
    <div className="flex flex-grow flex-col text-sm text-white">
      <div className="flex p-4 bg-slate-700 gap-2">
        <div className="w-[50px]">#</div>
        <div className="w-[100px]">#Block</div>
        <div className="w-[250px]">Trx</div>
        <div className="w-[320px]">Recipient</div>
      </div>
      {loading ? (
        <div className="self-center">Loading...</div>
      ) : (
        transactions.map((x, i) => (
          <div key={x.hash} className="flex p-4 bg-slate-600 gap-2">
            <div className="w-[50px]">{i + 1}</div>
            <div className="w-[100px]">{x.blockNum}</div>
            <div
              className="w-[250px] text-ellipsis overflow-hidden"
              title={x.hash}
            >
              <a
                href={`${explorerUrl}/tx/${x.hash}`}
                target="_blank"
                rel="noreferrer"
                className="underline text-blue-500"
              >
                {x.hash}
              </a>
            </div>
            <div
              className="w-[320px] text-ellipsis overflow-hidden"
              title={x.to ?? undefined}
            >
              <a
                href={`${explorerUrl}/address/${x.to}`}
                target="_blank"
                rel="noreferrer"
                className="underline text-blue-500"
              >
                {x.to}
              </a>
            </div>
          </div>
        ))
      )}
    </div>
  );
}

export default TransactionList;
