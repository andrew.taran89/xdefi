import { useEffect, useState } from "react";
import { etherToUsd } from "@/utils/etherToUsd";
import Button from "@/components/Button";

interface UserProps {
  address?: `0x${string}`;
  price?: number;
  balance?: string;
  symbol?: string;
  connect?: () => void;
}

function User({ address, price, balance, symbol, connect }: UserProps) {
  const [hasMounted, setHasMounted] = useState(false);

  useEffect(() => {
    setHasMounted(true);
  }, []);

  if (!hasMounted) {
    return null;
  }

  return address ? (
    <div className="flex flex-col justify-center items-end text-sm font-medium text-white">
      <div>{address}</div>
      {Boolean(balance) && <div>{`Balance: ${balance} ${symbol}`}</div>}
      {Boolean(balance && price) && <div>{etherToUsd(balance!, price!)}</div>}
    </div>
  ) : (
    <Button onClick={connect}>Connect wallet</Button>
  );
}

export default User;
