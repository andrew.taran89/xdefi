import { render, screen } from "@testing-library/react";
import User from "../index";
import "@testing-library/jest-dom";

describe("User", () => {
  it("renders user address", () => {
    const address = "0xb3f723783e67471F2469C3fc49E2905d5FabeE1d";
    render(<User address={address} />);

    const addressText = screen.getByText(
      "0xb3f723783e67471F2469C3fc49E2905d5FabeE1d"
    );

    expect(addressText).toBeInTheDocument();
  });

  it("renders connect button", () => {
    render(<User address={undefined} />);

    const connectButton = screen.getByRole("button");
    const buttonText = connectButton.textContent;

    expect(connectButton).toBeInTheDocument();
    expect(buttonText).toEqual("Connect wallet");
  });

  it("renders user ether balance ", () => {
    render(
      <User
        address="0xb3f723783e67471F2469C3fc49E2905d5FabeE1d"
        balance="1000"
        symbol="ETH"
      />
    );

    const balance = screen.getByText("Balance: 1000 ETH");

    expect(balance).toBeInTheDocument();
  });
});
