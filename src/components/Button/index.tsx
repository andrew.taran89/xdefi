import type { InputHTMLAttributes, PropsWithChildren } from "react";
import React from "react";

interface ButtonProps extends InputHTMLAttributes<HTMLButtonElement> {
  type?: "submit" | "button";
}

function Button({
  children,
  type = "button",
  className,
  ...rest
}: PropsWithChildren<ButtonProps>) {
  return (
    <button
      type={type === "button" ? "button" : "submit"}
      className="inline-flex items-center justify-center gap-2 rounded border p-2.5 px-5 text-center text-sm font-medium focus:border-blue-500 focus:outline-none focus:ring-blue-500 border-gray-700 bg-gray-800 text-white hover:bg-gray-700"
      {...rest}
    >
      {children}
    </button>
  );
}

export default Button;
