const config = {
  explorerUrl: process.env.NEXT_PUBLIC_EXPLORER_URL as string,
  alchemyApiKey: process.env.NEXT_PUBLIC_ALCHEMY_API_KEY as string,
  tokenVestingAddress: process.env
    .NEXT_PUBLIC_TOKEN_VESTING_CONTRACT as `0x${string}`,
};

export default config;
