import { useContract, useSigner } from "wagmi";
import abi from "@/lib/tokenVesting";
import config from "@/config";

const useTokenVesting = () =>
  useContract({
    abi,
    address: config.tokenVestingAddress,
    signerOrProvider: useSigner().data,
  });

export default useTokenVesting;
