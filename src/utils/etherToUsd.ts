export const etherToUsd = (ether: string, price: number) => {
  const summ = parseFloat(ether) * price;

  return new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  }).format(summ);
};
