import type { AppProps } from "next/app";
import { WagmiConfig } from "wagmi";
import { client } from "@/services/wagmi";
import "@/styles/globals.css";

export default function App({ Component, pageProps }: AppProps) {
  return (
    <WagmiConfig client={client}>
      <Component {...pageProps} />
    </WagmiConfig>
  );
}
