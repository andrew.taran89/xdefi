import { useEffect } from "react";
import { BigNumber, utils } from "ethers";
import { useAccount, useBalance, useConnect } from "wagmi";
import Button from "@/components/Button";
import Head from "@/components/Head";
import User from "@/components/User";
import TransactionList from "@/components/TransactionList";
import useTokenVesting from "@/hooks/useTokenVesting";
import usePriceStore from "@/models/price";
import useTransactionStore from "@/models/transactions";
import useTokensStore from "@/models/tokens";
import config from "@/config";

function Home() {
  const { connect, connectors } = useConnect();
  const [injectedConnector] = connectors;
  const { address } = useAccount();
  const { data: balance } = useBalance({ address });
  const contract = useTokenVesting();
  const { price, fetch: fetchPrice } = usePriceStore();
  const {
    tokens,
    loading: tokensLoading,
    setLoading,
    setTokens,
  } = useTokensStore();
  const {
    transactions,
    loading: transactionsLoading,
    fetch: fetchTransactions,
  } = useTransactionStore();

  const handleVestedClick = async () => {
    if (!address) return;
    try {
      setLoading(true);
      const data = await contract?.totalVestingsTokens();
      if (data) {
        setTokens(utils.formatEther(data.toString()));
      }
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  };

  const handleClaimClick = async () => {
    if (!address) return;
    try {
      await contract?.claimTokens(address, {
        gasLimit: BigNumber.from(100000),
      });
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchPrice();
  }, []);

  useEffect(() => {
    fetchTransactions(address);
  }, [address]);

  return (
    <>
      <Head />
      <div className="flex flex-grow flex-col overflow-hidden bg-gray-900">
        <div className="flex h-20 p-4 border-b border-gray-700 bg-gray-800 justify-end">
          <User
            address={address}
            price={price}
            balance={balance?.formatted}
            symbol={balance?.symbol}
            connect={() => connect({ connector: injectedConnector })}
          />
        </div>
        <div className="flex flex-grow overflow-hidden">
          <div className="flex flex-col gap-5 p-4 min-w-max bg-gray-800">
            <Button onClick={handleVestedClick}>Vested tokens</Button>
            <div className="flex items-center justify-center h-10 text-sm font-medium text-white">
              {tokensLoading
                ? "Loading..."
                : Boolean(tokens) && <div>{tokens} ETH</div>}
            </div>

            <Button onClick={handleClaimClick}>Claim tokens</Button>
          </div>
          <TransactionList
            loading={transactionsLoading}
            transactions={transactions}
            explorerUrl={config.explorerUrl}
          />
        </div>
      </div>
    </>
  );
}

export default Home;
